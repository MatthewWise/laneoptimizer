import operator, math

class PhaseSet(object):

    def __init__(self, name,total_volume, lanes=1):
        self._name=name
        self._total_volume=total_volume
        self._lanes=lanes
        self._effective_volume=total_volume/lanes

    @property
    def name(self):
        return self._name

    @property
    def total_volume(self):
        return self._total_volume

    @property
    def lanes(self):
        return self._lanes

    @property
    def effective_volume(self):
        return self._effective_volume

    def add_lane(self):
        return PhaseSet(total_volume=self.total_volume,lanes=self.lanes+1,name=self.name)

    def __copy__(self):
        return PhaseSet(name=self.name,total_volume=self.total_volume,lanes=self.lanes)

    def __add__(self, other):
        assert isinstance(other,PhaseSet)
        name=self.name+' and '+other.name
        return PhaseSet(total_volume=self.total_volume+other.total_volume,lanes=self.lanes+other.lanes, name=name)

    def __str__(self):
        return "{}: {} lane(s) splitting {} VPH for an effective volume of {} VPH".format(self.name,self.lanes,self.total_volume,self.effective_volume)

class Intersection(object):

    def __init__(self,ps_list,phf,v_per_c,tl,hs,C=None):
        self._ps_list=ps_list
        self._phf=phf
        self._v_per_c=v_per_c
        self._tl=tl
        self._hs=hs
        if C is None:
            C=self.necessary_C()
        self._C=C

    def make_new(self,ps_list=None,phf=None,v_per_c=None,tl=None,hs=None,C=None):
        if ps_list is None:
            ps_list=self._ps_list
        if phf is None:
            phf=self.phf
        if v_per_c is None:
            v_per_c=self.v_per_c
        if tl is None:
            tl=self.tl
        if hs is None:
            hs=self.hs
        if C is None:
            C=self.C

        return Intersection(ps_list,phf,v_per_c,tl,hs,C)


    #region Properties
    @property
    def N(self):
        return len(self._ps_list)
    @property
    def C(self):
        return self._C
    @property
    def lanes(self):
        return sum(map(lambda ps:ps.lanes,self._ps_list))
    @property
    def ps_list(self):
        return tuple(self._ps_list)
    @property
    def phf(self):
        return self._phf
    @property
    def v_per_c(self):
        return self._v_per_c
    @property
    def tl(self):
        return self._tl
    @property
    def hs(self):
        return self._hs
    @property
    def vcrit(self):
        return sum(map(lambda ps:ps.effective_volume,self._ps_list))
    @property
    def adjusted_vcrit(self):
        return self.vcrit/(self.phf*self.v_per_c)
    # endregion

    def __str__(self):
        prefix="\n\n Intersection: C={}, Lanes={}, Vcrit={}\n".format(self.C,self.lanes,self.adjusted_vcrit)
        middle="\n".join((ps.__str__() for ps in self._ps_list))

        return prefix+middle

    def necessary_C(self):
        return -(3600 * self.tl * self.N) / (self.hs * self.adjusted_vcrit - 3600)

    def round_green_cycle(self):
        #rounds cycle length up to next 5 seconds for neatness.
        self._C=5*(math.ceil(self.C/5.0))

    def add_best_lane(self):
        #Determines where adding a new lane will have the biggest impact and adds one there
        present_effective_volumes=[ps.effective_volume for ps in self._ps_list]
        new_effective_volumes=[ps.add_lane().effective_volume for ps in self._ps_list]
        change_in_effective_volumes=list(map(operator.sub,present_effective_volumes,new_effective_volumes))
        best_lane=change_in_effective_volumes.index(max(change_in_effective_volumes))
        new_ps_list=self._ps_list[:]
        new_ps_list[best_lane]=new_ps_list[best_lane].add_lane()
        return Intersection(new_ps_list,self.phf,self.v_per_c,self.tl,self.hs)

def optimize_lanes(intersection):
    present_intersection=intersection
    print(intersection)
    while present_intersection.C>120 or present_intersection.C<0:
        present_intersection=present_intersection.add_best_lane()
        print(present_intersection)
    return present_intersection

def split_the_green(intersection, yellow=2, all_red=2):
    t_min=yellow+all_red
    total_volume=sum((ps.effective_volume for ps in intersection.ps_list))
    splittable_time=intersection.C-(intersection.N*t_min)
    gs=[(ps.effective_volume/total_volume)*splittable_time for ps in intersection.ps_list]
    current_time=0
    representation=[]
    for ps, green in zip(intersection.ps_list,gs):
        representation.append("{} green starts at {}".format(ps.name,current_time))
        current_time+=green
        representation.append("{} yellow starts at {}".format(ps.name,current_time))
        current_time+=yellow
        representation.append("All red following {} starts at {}".format(ps.name, current_time))
        current_time+=all_red
    return '\n\n'+"\n".join(representation)




if __name__ == '__main__':
    ps_list=[
        PhaseSet('NS', 512),
        PhaseSet('EW', 280),
    ]
    intersection=Intersection(ps_list, phf=0.75, v_per_c=0.7, tl=8.5, hs=2.25)
    intersection=optimize_lanes(intersection)
    intersection.round_green_cycle()
    print(split_the_green(intersection))
